import React from 'react';
import './style.css';

const MesageInput = ({
  inputVal,
  onInputChange,
  onSend,
  isEditingMessage,
  onEditEnd
}) => {
  return (
    <div className="send-message">
      <div className="container">
        <input value={inputVal} onChange={onInputChange} />
        <button onClick={isEditingMessage ? onEditEnd : onSend}>
          {isEditingMessage ? 'Save' : 'Send'}
        </button>
      </div>
    </div>
  );
}

export default MesageInput;