import React from 'react';
import './style.css';
import moment from 'moment';

const Message = ({
  messageObj,
  onDeleteMessage,
  onEditClick,
  onAddLike
}) => {
  const { avatar, created_at, likes, id, message, user } = messageObj;

  const printMessage = () => (
    <div className="message">
      <img className="avatar" src={avatar} alt={user} />
      <div className="message-wrapper">
        <p>{message}</p>
        <div>
          <p>{moment(created_at).format("Do MM, HH:MM")}</p>
          <div className="like">
            <div onClick={() => onAddLike(id)} />
            <p>{likes}</p>
          </div>
        </div>
      </div>
    </div>
  );

  const printMyMessage = () => (
    <div className="message me">
      <div className="avatar" />
      <div className="message-wrapper">
        <p>{message}</p>
        <div>
          <p>{moment(created_at).format("Do MM, HH:MM")}</p>
          <div className="like">
            <div />
            <p>{likes}</p>
          </div>
          <div className="message-footer">
            <p onClick={() => onEditClick(message, id)}>edit</p>
            <p onClick={() => onDeleteMessage(id)}>delete</p>
          </div>
        </div>
      </div>
    </div>
  )

  if (user === 'Me') {
    return printMyMessage();
  } else {
    return printMessage();
  }
};

export default Message;