import React from 'react';
import './style.css';
import Header from '../Header';
import MesageList from '../MesageList';
import MesageInput from '../MesageInput';

const ChatView = ({
  messages,
  headerData,
  addNewMessage,
  onDeleteMessage,
  onEditClick,
  inputVal,
  onInputChange,
  onSend,
  isEditingMessage,
  onEditEnd,
  onAddLike
}) => {
  return (
    <div>
      <Header headerData={headerData} />
      <MesageList
        onDeleteMessage={onDeleteMessage}
        messages={messages}
        onEditClick={onEditClick}
        onAddLike={onAddLike}
      />
      <MesageInput
        isEditingMessage={isEditingMessage}
        inputVal={inputVal}
        onInputChange={onInputChange}
        onSend={onSend}
        addNewMessage={addNewMessage}
        onEditEnd={onEditEnd}
      />
    </div>
  );
};

export default ChatView;