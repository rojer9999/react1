import React from 'react';
import './style.css';

const Header = ({ headerData }) => {
  const { totalUsers, totalMessages, lastMessageAt } = headerData;

  return (
    <div className="header">
      <div className="container">
        <div className="inner-container">
          <div>
            <div className="logo"></div>
            <h1>React-Chat</h1>
          </div>
          <div className="data">
            <div>{totalUsers} Users</div>
            <div>{totalMessages} Messages</div>
            <div>Last Message At {lastMessageAt}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;