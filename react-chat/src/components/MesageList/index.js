import React, { Fragment } from 'react';
import './style.css';
import Message from '../Message';
import moment from 'moment';

let divider = '';
let prevDivider = ''

const getDividerText = (message) => {
  const daysDiff = moment.utc(moment().diff(message.created_at)).format('DD') - 1;
  if (prevDivider === daysDiff) {
    return false;
  } else {
    prevDivider = daysDiff;
  }

  if (daysDiff === 1) {
    divider = 'Yesterday';
  } else {
    divider = moment(message.created_at).fromNow();
  }

  return <div>{divider}</div>
}

const MesageList = ({
  messages,
  onDeleteMessage,
  onEditClick,
  onAddLike
}) => {
  const printMessages = () => {
    return messages.map((message, i, arr) => {

      return (
        <Fragment key={message.id}>
          {getDividerText(message)}
          <Message
            onDeleteMessage={onDeleteMessage}
            messageObj={message}
            onEditClick={onEditClick}
            onAddLike={onAddLike}
          />
        </Fragment>
      )
    })
  }
  
  return (
    <div className="mesage-list">
      <div className="container">
        {printMessages()}
      </div>
    </div>
  );
};

export default MesageList;