import React, { Component } from 'react';
import Spinner from '../../components/Spinner';
import ChatView from '../../components/ChatView';
import moment from 'moment';

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      isFetching: true,
      inputVal: '',
      editMessageWithId: ''
    }

    this.getHeaderData = this.getHeaderData.bind(this);
    this.onDeleteMessage = this.onDeleteMessage.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onSend = this.onSend.bind(this);
    this.onEditEnd = this.onEditEnd.bind(this);
    this.onAddLike = this.onAddLike.bind(this);
  }

  onDeleteMessage = (id) => {
    const messages = this.state.messages.filter((message) => message.id !== id);
    this.setState(() => ({ messages }));
  }

  onEditClick = (message, id) => {
    this.setState(() => ({
      inputVal: message,
      editMessageWithId: id
    }))
  }

  onEditEnd = () => {
    const { messages, editMessageWithId, inputVal } = this.state;
    const editedMessages = messages.map(message => {
      if (editMessageWithId === message.id) {
        message.message = inputVal;
      }
      return message;
    });
    this.setState(() => ({
      messages: editedMessages,
      inputVal: '',
      editMessageWithId: ''
    }));
  }

  onInputChange(e) {
    e.preventDefault();
    this.setState({ inputVal: e.target.value });
  }

  onSend() {
    const { inputVal } = this.state;
    if (!inputVal.trim()) {
      return false;
    }

    const newMsg = {
      id: Date.now() + '',
      user: 'Me',
      avatar: '',
      created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
      message: inputVal,
      marked_read: false,
      likes: 0,
    }

    this.setState(({ messages }) => ({
      messages: [...messages, newMsg],
      inputVal: ''
    }));
  }
  
  onAddLike(id) {
    const storage = JSON.parse(localStorage.getItem('likes'));
    if (storage && storage.includes(id)) {
      return false;
    }

    if (Array.isArray(storage)) {
      storage.push(id);
      localStorage.setItem('likes', JSON.stringify(storage));
    } else {
      localStorage.setItem('likes', JSON.stringify([id]));
    }

    const messages = this.state.messages.map(message => {
      if (message.id === id) {
        message.likes += 1;
      }
      return message;
    });

    this.setState(() => ({ messages }));
  }

  getHeaderData() {
    const { messages } = this.state;

    const getTotalUsers = () => {
      const users = messages.map(message => message.user);
      return [...new Set(users)].length;
    };

    const getLastMessageAt = () => {
      let time = 0;
      messages.forEach(({ created_at }) => {
        const timestamp = new Date(created_at).getTime();
        if (time < timestamp) {
          time = timestamp;
        }
      });
      return moment(time).format("Do MM, HH:MM");
    }

    return {
      totalUsers: getTotalUsers(),
      totalMessages: messages.length,
      lastMessageAt: getLastMessageAt()
    }
  }

  componentDidMount() {
    const storage = JSON.parse(localStorage.getItem('likes'));

    const addLikeField = (msgs) => {
      return msgs.map(msg => {
        msg.likes = 0;
        if (storage && storage.includes(msg.id)) {
          msg.likes += 1;
        }
        return msg;
      });
    }    

    fetch('https://api.myjson.com/bins/1hiqin')
      .then(response => response.json())
      .then(messages => {
        this.setState(() => ({
          messages: addLikeField(messages),
          isFetching: false
        }))
      })
      .catch(() => this.setState(() => ({ isFetching: false })))
  }

  render() {
    const { isFetching, messages, inputVal, editMessageWithId } = this.state;
    console.log('messages :', messages);
    
    if (isFetching) {
      return <Spinner/>
    }
    
    return (
      <div>
        <ChatView
          messages={messages}
          headerData={this.getHeaderData()}
          addNewMessage={this.addNewMessage}
          onDeleteMessage={this.onDeleteMessage}
          onEditClick={this.onEditClick}
          inputVal={inputVal}
          onInputChange={this.onInputChange}
          onSend={this.onSend}
          isEditingMessage={!!editMessageWithId}
          onEditEnd={this.onEditEnd}
          onAddLike={this.onAddLike}
        />
      </div>
    );
  }
}

export default Chat;